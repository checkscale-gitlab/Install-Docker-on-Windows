Installing Docker on Windows
============================

Instructions on how to install Docker ("Docker Engine") - not "Docker Desktop for Windows" - on Windows.

Table of Contents
-----------------

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Installation Instructions](#installation-instructions)
  - [Docker](#docker)
  - [Linux Containers](#linux-containers)
    - [WSL2](#wsl2)
    - [LCOW](#lcow)
  - [Additional Components](#additional-components)
    - [buildx](#buildx)
    - [Docker Compose](#docker-compose)
    - [Docker Scan](#docker-scan)
    - [Kubernetes](#kubernetes)

Introduction
------------

The Internet is full of guides on how to install Docker on Windows. Unfortunately they are all basically repeating [Docker Inc.'s instructions on how to install _Docker Desktop for Windows_](https://docs.docker.com/docker-for-windows/install/), which is the wrong thing to do.

_Docker Desktop_ has many disadvantages and almost no benefits compared to simply installing Docker (also called "Docker Engine" sometimes). Following are instruction on how to install Docker, not "Docker Desktop". For reasons why using _Docker Desktop_ is the wrong thing to do see [here](DockerDesktop.md).

Installing Docker is easy, but the instructions tend to move around and they're scattered among lots of places. This page is intended to save you the time locating them.

Prerequisites
-------------

**Note**: This section is not comprehensive. Required versions change with time, etc.

Basically the requirements are the same as you'd find online in the Microsoft, Docker or Mirantis websites and if you're set to install _Docker Desktop_ this should be enough.

For Windows containers you need at least:

1. Windows 10 64-bit Pro, Enterprise or Education.
2. _Containers_ Windows feature enabled.

For using Hyper-V isolation rather than process isolation you also need:

3. _Hyper-V_ Windows feature enabled and running.
   - This includes enabling hardware-assisted virtualization (VT-x / AMD-V) in the firmware configuration and not disabling Hyper-V in the BCD.

For Linux Containers using WSL2 you need:

1. Windows 10 64-bit of any edition, including Home.
2. WSL2 installed. See instructions on the [Microsoft website](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

Installation Instructions
-------------------------

If you want to skip reading and just get the commands see the [script](Install.ps1).

Assuming you have all the prerequisites, it will install Docker and the additional components mentioned here, except for the WSL2 components.

### Docker ###

To install Docker:

1. Download <https://repos.mirantis.com/win/static/stable/x86_64/docker-latest.zip>  (or a specific version if you prefer).

2. Extract to `%ProgramFiles%\Docker`

3. Add `%ProgramFiles%\Docker` to `%PATH%`.  
   **Note**: If you're only running Windows Home edition (or if you're interested in Linux containers _only_ for another reason) stop here and skip to the [WSL2](#wsl2) section below.

4. Run `dockerd.exe --register-service` as administrator ("elevated").  
   You might want to add `-G group_name` so non-admins can use Docker too. Alternatively you can configure it in the `daemon.json` file. See below.

5. Optional: Add a `C:\ProgramData\Docker\config\daemon.json` file. I like to add at least `"exec-opts":["isolation=process"]` to make process isolation the default. My file is:

    ```json
    {
        "debug": true,
        "exec-opts":["isolation=process"],
        "experimental": true,
        "group": "group_name"
    }
    ```

And you're done.

If you want Linux containers or additional components (such as `docker-compose.exe` or `kubectl.exe`) refer to the [relevant sections below](#additional-components).

### Linux Containers ###

There are two main ways to run Linux containers on Windows hosts other than setup a Linux VM of your choice and run Docker inside it.

The supported and recommended way is to use WSL2.

LCOW is unsupported and hasn't been update for a while, but it still works.

#### WSL2 ####

> For complete details see the page on [Installing Docker on WSL2](WSL2.md).

The steps are basically:

1. Install Docker on a WSL2 distribution using the distribution's package manager or by downloading the binaries.
2. Expose the Docker daemon to Windows
3. Configure the Docker client on Windows to access the Docker daemon on WSL2.
4. Configure Docker to start on user logon or system start.

#### LCOW ####

LCOW is unsupported and unless you rebase and patch the kernel yourself you'll stay with the LCOW kernel Microsoft released on November 2018, but it still works.

To install LCOW:

1. Download the most recent release from <https://github.com/linuxkit/lcow/releases> and extract the ZIP to `C:\Program Files\Linux Containers`, like so:

    ```powershell
    Invoke-WebRequest "https://github.com/linuxkit/lcow/releases/download/v4.14.35-v0.3.9/release.zip" -OutFile "release-v4.14.35-v0.3.9.zip"
    Remove-Item "$env:ProgramFiles\Linux Containers" -Force -Recurse
    Expand-Archive "release-v4.14.35-v0.3.9.zip" -DestinationPath "$Env:ProgramFiles\Linux Containers\."
    Remove-Item "release-v4.14.35-v0.3.9.zip"
    ```

2. Make sure you run the Docker daemon in experimental mode. The easy way is to add `"experimental": true` to the `daemon.json` file. If you copied my example from above it's already there.  
   Alternatively you can add `--experimental` to the `ImagePath` Registry value under `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\docker`.

You can now use Linux containers without doing anything special. You don't even have to add `--platform=linux` most of the time. For example, the following commands simply work:

```cmd
docker run --rm -it busybox
docker run --rm -it hello-world:linux
```

As of Docker 20 (March 2021) if you don't specify `--platform linux` you'll get a warning, but it still works:

```ps_output
PS D:\> docker run --rm -it alpine
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
ba3557a56b15: Pull complete
Digest: sha256:a75afd8b57e7f34e4dad8d65e2c7ba2e1975c795ce1ee22fa34f8cf46f96a3be
Status: Downloaded newer image for alpine:latest
WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (windows/amd64) and no specific platform was requested
/ # exit
PS D:\>
```

### Additional Components ###

### buildx ###

Download the latest release from  <https://github.com/docker/buildx/releases/>.

Using the GitHub API to get the latest version it would be something like:

```powershell
$DockerBuildxURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/buildx/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -like "buildx-*.windows-amd64.exe" | Select-Object -ExpandProperty browser_download_url

Invoke-WebRequest $DockerBuildxURL -OutFile "$Env:ProgramFiles\Docker\cli-plugins\docker-buildx.exe"
```

#### Docker Compose ####

Download the latest release from <https://github.com/docker/compose/releases/>.

Using the GitHub API to get the latest version it would be something like:

```powershell
$DockerComposeURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/compose/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -eq "docker-compose-Windows-x86_64.exe" | Select-Object -ExpandProperty browser_download_url

Invoke-WebRequest $DockerComposeURL -OutFile "$Env:ProgramFiles\Docker\cli-plugins\docker-compose.exe"
```

#### Docker Scan ####

Download the latest release from <https://github.com/docker/docker-scan-cli/releases/>.

Using the GitHub API to get the latest version it would be something like:

```powershell
$DockerScanURL = Invoke-WebRequest -Uri "https://api.github.com/repos/docker/scan-cli-plugin/releases/latest" | Select-Object -ExpandProperty Content | ConvertFrom-Json | Select-Object -ExpandProperty assets | Where-Object -Property name -eq "docker-scan_windows_amd64.exe" | Select-Object -ExpandProperty browser_download_url

Invoke-WebRequest $DockerScanURL -OutFile "$PATH_DOCKER_BIN\cli-plugins\docker-scan.exe"
```

#### Kubernetes ####

Download the latest release of `kubectl.exe` from Google. As of the time of writing this is something like:

```powershell
Invoke-WebRequest "https://storage.googleapis.com/kubernetes-release/release/v1.20.5/bin/windows/amd64/kubectl.exe" -OutFile "$Env:ProgramFiles\Docker\kubectl.exe"
```

If you want to automatically download the latest version you can use:

```powershell
Invoke-WebRequest "https://storage.googleapis.com/kubernetes-release/release/stable.txt"  | select -ExpandProperty Content | % { Invoke-WebRequest ("https://storage.googleapis.com/kubernetes-release/release/" + $_ + "/bin/windows/amd64/kubectl.exe") -OutFile "$Env:ProgramFiles\Docker\kubectl.exe" }
```

You can also a few other ways (such as Chocolatey and others) mentioned in <https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-windows>.
